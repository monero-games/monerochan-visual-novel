Monerochan:
- Object:
  - Background:image
  - Character:image Monerochan
Hi, I'm Monerochan.
Nice to meet you.
Who are you?

You: ...

Monerochan:
Why so shy?
Let me take a seat.
- Image:Monerochan:pose Monerochan_Logo.png

Monerochan:
I'm on the run and would like some company on my journey.
Especially from a /strong guy like you/.

Monerochan:
Would you like to accompany me?
You don't look like you have anything else to do today.

You:
I really could need some vacation.
So where do you want to go?

Monerochan:
It's a ^|secret|^.
But I'm sure, you'll like it there.
- Image:Monerochan:pose Monerochan_stretches.png
Let's go!

- Image:Monerochan:pose Monerochan_Logo.png
Monerochan leaves the house and you follow her.

Monerochan:
- Object:Character:image Monerochan
What are you waiting for?
*Hurry up!*

- Object:Character:image!
You're already looking forward to your adventure.
