# Installation

First you need to install [VNgine](https://gitlab.com/porky11/vngine-rs), a Visual Novel engine.

You can easily install `vngine` using the following command if you have `cargo` installed:

```bash
cargo install vngine
```

You also can find some binaries [here](https://f-u-k.xyz/VNgine.html).

# Running

Run the file `Monerochan.vng` directly or open the file `Monerochan.vng` using `vngine`.

When using the command line, you can also run `vngine Monerochan.vng`.

# Contributing

Feel free to contribute.

You might consider to read about [VNgine](https://gitlab.com/porky11/vngine-rs) first.
